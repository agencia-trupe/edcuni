<a href="" class="link-nav nav-nossos-servicos">
    {{ trans('frontend.geral.nossos-servicos') }}
    <hr class="linha-hover">
</a>
<a href="" class="link-nav" id="navPessoas" submenu="submenu-pessoas">
    {{ trans('frontend.geral.pessoas') }}
    <hr class="linha-active">
</a>
<a href="" class="link-nav" id="navEstrategia" submenu="submenu-estrategia">
    {{ trans('frontend.geral.estrategia') }}
    <hr class="linha-active">
</a>
<a href="" class="link-nav nav-quem-faz">
    {{ trans('frontend.geral.quem-faz') }}
    <hr class="linha-hover">
</a>
<a href="" class="link-nav nav-contato">
    {{ trans('frontend.geral.contato') }}
    <hr class="linha-hover">
</a>

<!-- submenus -->
<div class="submenu-nav submenu-pessoas" style="display: none;">
    @foreach($gruposPessoas as $grupo)
    @if(Lang::getLocale() == "en")
    <a href="{{ route('grupos-en', $grupo->slug_en) }}" class="link-submenu @if(url()->current() == route('grupos-en', $grupo->slug_en)) active @endif">{{ $grupo->{trans('database.titulo')} }}</a>
    @elseif(Lang::getLocale() == "es")
    <a href="{{ route('grupos-es', $grupo->slug_es) }}" class="link-submenu @if(url()->current() == route('grupos-es', $grupo->slug_es)) active @endif">{{ $grupo->{trans('database.titulo')} }}</a>
    @else
    <a href="{{ route('grupos', $grupo->slug_pt) }}" class="link-submenu @if(url()->current() == route('grupos', $grupo->slug_pt)) active @endif">{{ $grupo->{trans('database.titulo')} }}</a>
    @endif
    @endforeach
</div>

<div class="submenu-nav submenu-estrategia" style="display: none;">
    @foreach($gruposEstrategia as $grupo)
    @if(Lang::getLocale() == "en")
    <a href="{{ route('grupos-en', $grupo->slug_en) }}" class="link-submenu @if(url()->current() == route('grupos-en', $grupo->slug_en)) active @endif">{{ $grupo->{trans('database.titulo')} }}</a>
    @elseif(Lang::getLocale() == "es")
    <a href="{{ route('grupos-es', $grupo->slug_es) }}" class="link-submenu @if(url()->current() == route('grupos-es', $grupo->slug_es)) active @endif">{{ $grupo->{trans('database.titulo')} }}</a>
    @else
    <a href="{{ route('grupos', $grupo->slug_pt) }}" class="link-submenu @if(url()->current() == route('grupos', $grupo->slug_pt)) active @endif">{{ $grupo->{trans('database.titulo')} }}</a>
    @endif
    @endforeach
</div>