@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>CAPAS |</small> Editar Capa</h2>
</legend>

{!! Form::model($capa, [
'route' => ['capas.update', $capa->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.home.capas.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection