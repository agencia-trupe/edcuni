<?php

namespace App\Models;

use App\Helpers\CropImage;
use App\Helpers\CropImageTinify;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Capa extends Model
{
    use HasFactory;

    protected $table = 'capas';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_capa()
    {
        if (Configuracao::first()->tinify_key) {
            return CropImageTinify::make('capa', [
                'width'  => 450,
                'height' => 450,
                'path'   => 'assets/img/home/capas/'
            ]);
        } else {
            return CropImage::make('capa', [
                'width'  => 450,
                'height' => 450,
                'path'   => 'assets/img/home/capas/'
            ]);
        }
    }
}
