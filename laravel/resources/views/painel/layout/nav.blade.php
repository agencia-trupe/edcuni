<ul class="nav navbar-nav">

    <li class="nav-item dropdown">
        <a href="#" class="nav-link dropdown-toggle px-3 @if(Tools::routeIs(['capas*', 'home*'])) active @endif" role="button" id="navbarDarkDropdownMenuHome" data-bs-toggle="dropdown" aria-expanded="false">
            Home <i class="bi bi-caret-down-fill ms-1"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuHome">
            <li>
                <a href="{{ route('home.index') }}" class="dropdown-item @if(Tools::routeIs('home*')) active @endif">Home</a>
            </li>
            <li>
                <a href="{{ route('capas.index') }}" class="dropdown-item @if(Tools::routeIs('capas*')) active @endif">Capas</a>
            </li>
        </ul>
    </li>

    <li>
        <a href="{{ route('grupos.index') }}" class="nav-link px-3 @if(Tools::routeIs('grupos*')) active @endif">Grupos</a>
    </li>

    <li>
        <a href="{{ route('empresas-grupo.index') }}" class="nav-link px-3 @if(Tools::routeIs('empresas-grupo*')) active @endif">Empresas [Grupo EDC]</a>
    </li>

    <li class="nav-item dropdown">
        <a href="#" class="nav-link dropdown-toggle px-3 @if(Tools::routeIs(['contatos*', 'trabalhe-conosco*'])) active @endif d-flex align-items-center" role="button" id="navbarDarkDropdownMenuContatos" data-bs-toggle="dropdown" aria-expanded="false">
            Contatos
            @php $total = $contatosNaoLidos + $curriculosNaoLidos @endphp
            @if($total >= 1)
            <span class="label label-success ms-1">{{ $total }}</span>
            @endif
            <i class="bi bi-caret-down-fill ms-1"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuContatos">
            <li>
                <a href="{{ route('contatos.index') }}" class="dropdown-item @if(Tools::routeIs('contatos.index')) active @endif">Informações de contato</a>
            </li>
            <li>
                <a href="{{ route('contatos-recebidos.index') }}" class="dropdown-item @if(Tools::routeIs('contatos-recebidos*')) active @endif d-flex align-items-center">
                    Contatos recebidos
                    @if($contatosNaoLidos >= 1)
                    <span class="label label-success ms-1">{{ $contatosNaoLidos }}</span>
                    @endif
                </a>
            </li>
            <li>
                <a href="{{ route('trabalhe-conosco.index') }}" class="dropdown-item @if(Tools::routeIs('trabalhe-conosco*')) active @endif d-flex align-items-center">
                    Trabalhe Conosco
                    @if($curriculosNaoLidos >= 1)
                    <span class="label label-success ms-1">{{ $curriculosNaoLidos }}</span>
                    @endif
                </a>
            </li>
        </ul>
    </li>

</ul>