@extends('painel.layout.template')

@section('content')

@include('painel.layout.flash')

<legend class="mb-4">
    <h2 class="m-0">TRABALHE CONOSCO</h2>
</legend>

@if(!count($curriculos))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="trabalhe_conosco">
        <thead>
            <tr>
                <th scope="col">Data</th>
                <th scope="col">Nome</th>
                <th scope="col">E-mail</th>
                <th scope="col">Arquivo</th>
                <th class="no-filter" scope="col"><i class="bi bi-gear-fill me-2"></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($curriculos as $curriculo)
            <tr class="@if(!$curriculo->lido)alert alert-warning @endif" id="{{ $curriculo->id }}">
                <td data-order="{{ $curriculo->created_at_order }}">{{ $curriculo->created_at }}</td>
                <td>{{ $curriculo->nome }}</td>
                <td class="d-flex flex-row align-items-center">
                    <button class="btn btn-dark btn-sm clipboard me-2" data-clipboard-text="{{ $curriculo->email }}">
                        <i class="bi bi-clipboard"></i>
                    </button>
                    {{ $curriculo->email }}
                </td>
                <td>
                    <a href="{{ route('trabalhe-conosco.arquivo', ['id' => $curriculo->id]) }}" target="_blank">{{ $curriculo->arquivo }}</a>
                </td>
                <td class="crud-actions">
                    {!! Form::open([
                    'route' => ['trabalhe-conosco.destroy', $curriculo->id],
                    'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm" role="group">
                        <a href="{{ route('trabalhe-conosco.show', $curriculo->id ) }}" class="btn btn-primary btn-sm">
                            <i class="bi bi-chat-text-fill me-2"></i>Ler mensagem
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><i class="bi bi-trash-fill me-2"></i>Excluir</button>

                        <a href="{{ route('trabalhe-conosco.toggle', $curriculo->id) }}" class="btn btn-sm {{ ($curriculo->lido ? 'btn-warning' : 'btn-success') }}">
                            @if($curriculo->lido)
                            <i class="bi bi-arrow-repeat"></i>
                            @else
                            <i class="bi bi-check2-circle"></i>
                            @endif
                        </a>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endif

@endsection