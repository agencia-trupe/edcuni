<section class="grupo-edc">
    <div class="center">
        <p class="frase-grupo">{{ trans('frontend.home.frase-grupo') }}</p>
        <article class="grupo">
            @foreach($empresas as $empresa)
            <a href="{{ $empresa->link }}" target="_blank" class="@if($empresa->id == 1) edc-servicos @elseif($empresa->id == 2) edc-engenharia @else edc-uni @endif">
                <img src="{{ asset('assets/img/empresas/'.$empresa->logo) }}" class="img-logo" title="{{ $empresa->{trans('database.nome')} }}">
                <div class="servicos">
                    {!! $empresa->{trans('database.servicos')} !!}
                    <hr class="linha-divisao l1">
                    <hr class="linha-divisao l2">
                </div>
                <p class="@if($empresa->id == 1) link-servicos @elseif($empresa->id == 2) link-engenharia @else link-uni @endif">
                    {{ trans('frontend.home.visite-website') }}
                </p>
            </a>
            @endforeach
        </article>
    </div>
</section>