@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0">TRABALHE CONOSCO</h2>
</legend>

<div class="mb-3 col-12">
    <label>Data</label>
    <div class="well" >{{ $curriculo->created_at }}</div>
</div>

<div class="mb-3 col-12">
    <label>Nome</label>
    <div class="well">{{ $curriculo->nome }}</div>
</div>

<div class="mb-3 col-12">
    <label>E-mail</label>
    <div class="well">
        <button class="btn btn-dark btn-sm clipboard me-2" data-clipboard-text="{{ $curriculo->email }}">
            <i class="bi bi-clipboard"></i>
        </button>
        {{ $curriculo->email }}
    </div>
</div>

@if($curriculo->telefone)
<div class="mb-3 col-12">
    <label>Telefone</label>
    <div class="well">{{ $curriculo->telefone }}</div>
</div>
@endif

<div class="mb-3 col-12">
    <label>Mensagem</label>
    <div class="well-msg">{!! $curriculo->mensagem !!}</div>
</div>

<div class="mb-3 col-12">
    <label>Arquivo</label>
    <div class="well">
        <a href="{{ route('trabalhe-conosco.arquivo', ['id' => $curriculo->id]) }}" target="_blank">{{ $curriculo->arquivo }}</a>
    </div>
</div>


<div class="d-flex align-items-center mt-4">
    <a href="{{ route('trabalhe-conosco.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>

@stop