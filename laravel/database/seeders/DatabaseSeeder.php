<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ConfiguracoesTableSeeder::class);
        $this->call(ContatosTableSeeder::class);
        $this->call(HomeTableSeeder::class);
        $this->call(EmpresasGrupoTableSeeder::class);
    }
}
