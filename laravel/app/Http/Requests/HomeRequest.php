<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HomeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'imagem_banner'      => 'required|image',
            'titulo_banner_pt'   => 'required',
            'frase_banner_pt'    => 'required',
            'btn1_link'          => 'required',
            'btn1_titulo_pt'     => 'required',
            'btn2_link'          => 'required',
            'btn2_titulo_pt'     => 'required',
            'texto_proposito_pt' => 'required',
            'imagem_edc'         => 'required|image',
            'titulo_edc_pt'      => 'required',
            'texto1_edc_pt'      => 'required',
            'texto2_edc_pt'      => 'required',
            'imagem_quantidades' => 'required|image',
            'item1_titulo_pt'    => 'required',
            'item1_quantidade'   => 'required',
            'item2_titulo_pt'    => 'required',
            'item2_quantidade'   => 'required',
            'item3_titulo_pt'    => 'required',
            'item3_quantidade'   => 'required',
            'item4_titulo_pt'    => 'required',
            'item4_quantidade'   => 'required',
            'frase_servicos_pt'  => 'required',
            'titulo_servicos_pt' => 'required',
            'titulo_quem_faz_pt' => 'required',
            'frase_quem_faz_pt'  => 'required',
            'imagem_quem_faz'    => 'required|image',
            'nome_quem_faz'      => 'required',
            'titulo_cargo_pt'    => 'required',
            'texto_quem_faz_pt'  => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
            $rules['imagem_banner'] = 'image';
            $rules['imagem_edc'] = 'image';
            $rules['imagem_quantidades'] = 'image';
            $rules['imagem_quem_faz'] = 'image';
        }

        return $rules;
    }
}
