<?php

namespace App\Http\Controllers;

use App\Models\Grupo;
use App\Models\Servico;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class GruposController extends Controller
{
    public function index($slug)
    {
        $grupo = Grupo::where('slug_pt', $slug)->first();
        if ($grupo == null) {
            $grupo = Grupo::where('slug_en', $slug)->first();
            if ($grupo == null) {
                $grupo = Grupo::where('slug_es', $slug)->first();
            }
        }
        
        $servicos = Servico::where('grupo_id', $grupo->id)->ordenados()->get();

        return view('frontend.grupos', compact('grupo', 'servicos'));
    }
}
