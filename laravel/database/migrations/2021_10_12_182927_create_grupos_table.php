<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGruposTable extends Migration
{
    public function up()
    {
        Schema::create('grupos', function (Blueprint $table) {
            $table->id();
            $table->integer('ordem')->default(0);
            $table->string('icone');
            $table->string('slug_pt');
            $table->string('slug_en')->nullable();
            $table->string('slug_es')->nullable();
            $table->string('titulo_pt');
            $table->string('titulo_en')->nullable();
            $table->string('titulo_es')->nullable();
            $table->string('frase_pt');
            $table->string('frase_en')->nullable();
            $table->string('frase_es')->nullable();
            $table->text('texto_pt');
            $table->text('texto_en')->nullable();
            $table->text('texto_es')->nullable();
            $table->text('texto_home_pt');
            $table->text('texto_home_en')->nullable();
            $table->text('texto_home_es')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('grupos');
    }
}
