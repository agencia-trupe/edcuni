@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>GRUPO: {{ $grupo->titulo_pt }} |</small> Editar Serviço</h2>
</legend>

{!! Form::model($servico, [
'route' => ['grupos.servicos.update', $grupo->id, $servico->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.grupos.servicos.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection