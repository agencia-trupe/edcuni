@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>GRUPOS |</small> Editar Grupo</h2>
</legend>

{!! Form::model($grupo, [
'route' => ['grupos.update', $grupo->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.grupos.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection