<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContatosTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('contatos')->insert([
            'email'           => 'contato@trupe.net',
            'telefone'        => '11 3292 6444',
            'celular'         => '11 91308 7765',
            'atendimento_pt'  => 'segunda a sexta das 9h às 18h',
            'atendimento_en'  => '',
            'atendimento_es'  => '',
            'endereco_pt1_pt' => 'SEDE • Rua José Bonifácio, 24 - 1º andar',
            'endereco_pt1_en' => '',
            'endereco_pt1_es' => '',
            'endereco_pt2_pt' => 'Sé - São Paulo, SP • Brasil • 01003-900',
            'endereco_pt2_en' => '',
            'endereco_pt2_es' => '',
            'frase_pt'        => 'Fale com um de nossos especialistas e peça sua proposta.',
            'frase_en'        => '',
            'frase_es'        => '',
            'facebook'        => '',
            'linkedin'        => '',
            'instagram'       => '',
            'link_vagas'      => '',
            'imagem'          => '',
            'google_maps'     => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3657.5446474516048!2d-46.636790684615136!3d-23.548874684689107!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce59ab2647593f%3A0x207dd8a82eeee391!2zUi4gSm9zw6kgQm9uaWbDoWNpbywgMjQgLSBTw6ksIFPDo28gUGF1bG8gLSBTUCwgMDEwMDMtMDAw!5e0!3m2!1spt-BR!2sbr!4v1632840664723!5m2!1spt-BR!2sbr" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>',
        ]);
    }
}
