<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContatosTable extends Migration
{
    public function up()
    {
        Schema::create('contatos', function (Blueprint $table) {
            $table->id();
            $table->string('email');
            $table->string('telefone');
            $table->string('celular');
            $table->string('atendimento_pt');
            $table->string('atendimento_en')->nullable();
            $table->string('atendimento_es')->nullable();
            $table->string('endereco_pt1_pt');
            $table->string('endereco_pt1_en')->nullable();
            $table->string('endereco_pt1_es')->nullable();
            $table->string('endereco_pt2_pt');
            $table->string('endereco_pt2_en')->nullable();
            $table->string('endereco_pt2_es')->nullable();
            $table->string('frase_pt');
            $table->string('frase_en')->nullable();
            $table->string('frase_es')->nullable();
            $table->string('facebook')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('instagram')->nullable();
            $table->string('link_vagas');
            $table->string('imagem');
            $table->text('google_maps');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('contatos');
    }
}
