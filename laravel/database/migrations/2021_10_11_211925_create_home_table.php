<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHomeTable extends Migration
{
    public function up()
    {
        Schema::create('home', function (Blueprint $table) {
            $table->id();
            $table->string('imagem_banner');
            $table->string('titulo_banner_pt');
            $table->string('titulo_banner_en')->nullable();
            $table->string('titulo_banner_es')->nullable();
            $table->string('frase_banner_pt');
            $table->string('frase_banner_en')->nullable();
            $table->string('frase_banner_es')->nullable();
            $table->string('btn1_link');
            $table->string('btn1_titulo_pt');
            $table->string('btn1_titulo_en')->nullable();
            $table->string('btn1_titulo_es')->nullable();
            $table->string('btn2_link');
            $table->string('btn2_titulo_pt');
            $table->string('btn2_titulo_en')->nullable();
            $table->string('btn2_titulo_es')->nullable();
            $table->text('texto_proposito_pt');
            $table->text('texto_proposito_en')->nullable();
            $table->text('texto_proposito_es')->nullable();
            $table->string('imagem_edc');
            $table->string('titulo_edc_pt');
            $table->string('titulo_edc_en')->nullable();
            $table->string('titulo_edc_es')->nullable();
            $table->text('texto1_edc_pt');
            $table->text('texto1_edc_en')->nullable();
            $table->text('texto1_edc_es')->nullable();
            $table->text('texto2_edc_pt');
            $table->text('texto2_edc_en')->nullable();
            $table->text('texto2_edc_es')->nullable();
            $table->string('imagem_quantidades');
            $table->string('item1_titulo_pt');
            $table->string('item1_titulo_en')->nullable();
            $table->string('item1_titulo_es')->nullable();
            $table->integer('item1_quantidade');
            $table->string('item2_titulo_pt');
            $table->string('item2_titulo_en')->nullable();
            $table->string('item2_titulo_es')->nullable();
            $table->integer('item2_quantidade');
            $table->string('item3_titulo_pt');
            $table->string('item3_titulo_en')->nullable();
            $table->string('item3_titulo_es')->nullable();
            $table->integer('item3_quantidade');
            $table->string('item4_titulo_pt');
            $table->string('item4_titulo_en')->nullable();
            $table->string('item4_titulo_es')->nullable();
            $table->integer('item4_quantidade');
            $table->string('frase_servicos_pt');
            $table->string('frase_servicos_en')->nullable();
            $table->string('frase_servicos_es')->nullable();
            $table->string('titulo_servicos_pt');
            $table->string('titulo_servicos_en')->nullable();
            $table->string('titulo_servicos_es')->nullable();
            $table->string('titulo_quem_faz_pt');
            $table->string('titulo_quem_faz_en')->nullable();
            $table->string('titulo_quem_faz_es')->nullable();
            $table->string('frase_quem_faz_pt');
            $table->string('frase_quem_faz_en')->nullable();
            $table->string('frase_quem_faz_es')->nullable();
            $table->string('imagem_quem_faz');
            $table->string('nome_quem_faz');
            $table->string('titulo_cargo_pt');
            $table->string('titulo_cargo_en')->nullable();
            $table->string('titulo_cargo_es')->nullable();
            $table->text('texto_quem_faz_pt');
            $table->text('texto_quem_faz_en')->nullable();
            $table->text('texto_quem_faz_es')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('home');
    }
}
