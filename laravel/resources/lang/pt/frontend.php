<?php

return [
    '404' => 'Página não encontrada',

    'geral' => [
        'vagas'             => 'Vagas',
        'vagas-texto'       => 'Venha trabalhar com a gente. Confira nossas vagas',
        'area-do-candidato' => 'Área do Candidato',
        'area-do-consultor' => 'Área do Consultor',
        'termos'            => 'Termos de Uso',
        'politica'          => 'Política de Privacidade',
        'direitos'          => 'Todos os direitos reservados',
        'criacao'           => 'Criação de sites: ',
        'edc-group'         => 'É uma empresa do Grupo EDC',
        'nossos-servicos'   => 'NOSSOS SERVIÇOS',
        'pessoas'           => 'PESSOAS',
        'estrategia'        => 'ESTRATÉGIA',
        'quem-faz'          => 'QUEM FAZ',
        'contato'           => 'FALE COM UM ESPECIALISTA',
        'treinamentos'      => 'TREINAMENTOS',
        'trabalhe-conosco'  => 'TRABALHE CONOSCO',
        'selo-slac1'        => 'COACH RECONHECIDO E APROVADO',
        'selo-slac2'        => 'Sociedade Latino Americana de Coaching',
        'selo-iso1'         => 'Empresa com ',
        'selo-iso2'         => 'CERTIFICAÇÃO ISO 9001',
        'selo-iso3'         => 'Engenharia de Projetos e Mecânica',
        'selo-iso4'         => 'Serviços Temporários e Gestão de RH',
    ],

    'home' => [
        '+de'            => '+de',
        'frase-grupo'    => 'A EDC SERVIÇOS É UMA EMPRESA DA EDC GROUP • CONHEÇA AS EMPRESAS DO GRUPO:',
        'edc-servicos'   => 'EDC Serviços',
        'visite-website' => 'VISITE O WEBSITE',
        'edc-engenharia' => 'EDC Engenharia',
        'edc-uni'        => 'EDC UNI',
    ],

    'contato' => [
        'nome'        => 'nome',
        'telefone'    => 'telefone',
        'empresa'     => 'empresa',
        'mensagem'    => 'mensagem',
        'enviar'      => 'ENVIAR',
        'msg-sucesso' => 'Mensagem enviada com sucesso!',
        'anexar'      => 'anexar currículo',
        'selecionar'  => 'SELECIONAR ARQUIVO',
    ],

    'cookies1' => 'Usamos cookies para personalizar o conteúdo, acompanhar anúncios e oferecer uma experiência de navegação mais segura a você. Ao continuar navegando em nosso site você concorda com o uso dessas informações. Leia nossa ',
    'cookies2' => 'Política de Privacidade',
    'cookies3' => ' e saiba mais.',
    'btn-cookies' => 'ACEITAR E FECHAR',
];
