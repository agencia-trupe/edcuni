<?php

return [
    'title'            => 'title_pt',
    'description'      => 'description_pt',
    'capa'             => 'capa_pt',
    'slug'             => 'slug_pt',
    'titulo'           => 'titulo_pt',
    'frase'            => 'frase_pt',
    'texto'            => 'texto_pt',
    'imagem'           => 'imagem_pt',
    'atendimento'      => 'atendimento_pt',
    'endereco_pt1'     => 'endereco_pt1_pt',
    'endereco_pt2'     => 'endereco_pt2_pt',
    'frase_selo'       => 'frase_selo_pt',
    'titulo_capa'      => 'titulo_capa_pt',
    'frase_capa'       => 'frase_capa_pt',
    'titulo_banner'    => 'titulo_banner_pt',
    'frase_banner'     => 'frase_banner_pt',
    'btn1_titulo'      => 'btn1_titulo_pt',
    'btn2_titulo'      => 'btn2_titulo_pt',
    'texto_proposito'  => 'texto_proposito_pt',
    'titulo_edc'       => 'titulo_edc_pt',
    'texto1_edc'       => 'texto1_edc_pt',
    'texto2_edc'       => 'texto2_edc_pt',
    'item1_titulo'     => 'item1_titulo_pt',
    'item2_titulo'     => 'item2_titulo_pt',
    'item3_titulo'     => 'item3_titulo_pt',
    'item4_titulo'     => 'item4_titulo_pt',
    'frase_servicos'   => 'frase_servicos_pt',
    'titulo_servicos'  => 'titulo_servicos_pt',
    'titulo_quem_faz'  => 'titulo_quem_faz_pt',
    'frase_quem_faz'   => 'frase_quem_faz_pt',
    'titulo_cargo'     => 'titulo_cargo_pt',
    'texto_quem_faz'   => 'texto_quem_faz_pt',
    'texto_home'       => 'texto_home_pt',
    'nome'             => 'nome_pt',
    'servicos'         => 'servicos_pt',
];
