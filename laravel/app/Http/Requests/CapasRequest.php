<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CapasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'capa'          => 'required|image',
            'titulo_pt'     => 'required',
            'frase_pt'      => 'required',
        ];
    }
}
