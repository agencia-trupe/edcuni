@include('painel.layout.flash')

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('icone', 'Ícone') !!}
    @if($submitText == 'Alterar')
    @if($grupo->icone)
    <img src="{{ url('assets/img/grupos/'.$grupo->icone) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    @endif
    {!! Form::file('icone', ['class' => 'form-control']) !!}
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('titulo_pt', 'Título (PT)') !!}
        {!! Form::text('titulo_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('titulo_en', 'Título (EN)') !!}
        {!! Form::text('titulo_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('titulo_es', 'Título (ES)') !!}
        {!! Form::text('titulo_es', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase_pt', 'Frase (PT)') !!}
        {!! Form::text('frase_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase_en', 'Frase (EN)') !!}
        {!! Form::text('frase_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase_es', 'Frase (ES)') !!}
        {!! Form::text('frase_es', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('texto_pt', 'Texto (PT)') !!}
        {!! Form::textarea('texto_pt', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('texto_en', 'Texto (EN)') !!}
        {!! Form::textarea('texto_en', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('texto_es', 'Texto (ES)') !!}
        {!! Form::textarea('texto_es', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('texto_home_pt', 'Texto Home (PT)') !!}
        {!! Form::textarea('texto_home_pt', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('texto_home_en', 'Texto Home (EN)') !!}
        {!! Form::textarea('texto_home_en', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('texto_home_es', 'Texto Home (ES)') !!}
        {!! Form::textarea('texto_home_es', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('grupos.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>