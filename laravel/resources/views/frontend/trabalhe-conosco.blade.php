@extends('frontend.layout.template')

@section('content')

<main class="trabalhe-conosco">
    <h4 class="titulo">{{ trans('frontend.geral.trabalhe-conosco') }}</h4>
    <div class="form-contato">
        <img src="{{ asset('assets/img/contatos/'.$contato->imagem) }}" class="img-contato">
        <form action="{{ route('trabalhe-conosco.post') }}" method="POST" enctype="multipart/form-data">
            {!! csrf_field() !!}
            <input type="text" name="nome" placeholder="{{ trans('frontend.contato.nome') }}" value="{{ old('nome') }}" required>
            <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
            <input type="text" name="telefone" placeholder="{{ trans('frontend.contato.telefone') }}" class="input-telefone" value="{{ old('telefone') }}">
            <textarea name="mensagem" placeholder="{{ trans('frontend.contato.mensagem') }}" required>{{ old('mensagem') }}</textarea>
            <div class="anexar-arquivo">
                <p class="frase-anexar">{{ trans('frontend.contato.anexar') }}</p>
                <label for="files" class="btn-anexar">{{ trans('frontend.contato.selecionar') }}</label>
                <input type="file" id="files" name="arquivo" style="visibility:hidden; display:none;">
            </div>
            <button type="submit" class="btn-enviar">
                {{ trans('frontend.contato.enviar') }}
                <img src="{{ asset('assets/img/layout/seta-enviar.svg') }}" alt="" class="img-enviar">
            </button>

            @if($errors->any())
            <div class="flash flash-erro">
                @foreach($errors->all() as $error)
                {!! $error !!}<br>
                @endforeach
            </div>
            @endif

            @if(session('enviado'))
            <div class="flash flash-sucesso">
                <p>{{ trans('frontend.contato.msg-sucesso') }}</p>
            </div>
            @endif
        </form>
    </div>

    @include('frontend.grupo-edc')

</main>

@endsection