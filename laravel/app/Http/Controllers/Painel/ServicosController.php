<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ServicosRequest;
use App\Models\Grupo;
use App\Models\Servico;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ServicosController extends Controller
{
    public function index(Grupo $grupo)
    {
        $servicos = Servico::grupo($grupo->id)->ordenados()->get();
        // dd($servicos);

        return view('painel.grupos.servicos.index', compact('grupo', 'servicos'));
    }

    public function create(Grupo $grupo)
    {
        return view('painel.grupos.servicos.create', compact('grupo'));
    }

    public function store(ServicosRequest $request, Grupo $grupo)
    {
        try {
            $input = $request->all();

            $input['grupo_id'] = $grupo->id;

            $input['slug_pt'] = Str::slug($request->titulo_pt, "-");
            if (isset($input['titulo_en'])) $input['slug_en'] = Str::slug($request->titulo_en, "-");
            if (isset($input['titulo_es'])) $input['slug_es'] = Str::slug($request->titulo_es, "-");

            if (isset($input['capa'])) $input['capa'] = Servico::upload_capa();

            Servico::create($input);

            return redirect()->route('grupos.servicos.index', $grupo->id)->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Grupo $grupo, Servico $servico)
    {
        return view('painel.grupos.servicos.edit', compact('grupo', 'servico'));
    }

    public function update(ServicosRequest $request, Grupo $grupo, Servico $servico)
    {
        try {
            $input = $request->all();
            
            $input['grupo_id'] = $grupo->id;

            $input['slug_pt'] = Str::slug($request->titulo_pt, "-");
            if (isset($input['titulo_en'])) $input['slug_en'] = Str::slug($request->titulo_en, "-");
            if (isset($input['titulo_es'])) $input['slug_es'] = Str::slug($request->titulo_es, "-");

            if (isset($input['capa'])) $input['capa'] = Servico::upload_capa();

            $servico->update($input);

            return redirect()->route('grupos.servicos.index', $grupo->id)->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Grupo $grupo, Servico $servico)
    {
        try {
            $servico->delete();

            return redirect()->route('grupos.servicos.index', $grupo->id)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
