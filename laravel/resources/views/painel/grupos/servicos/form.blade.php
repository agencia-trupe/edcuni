@include('painel.layout.flash')

<div class="mb-3 col-12">
    {!! Form::label('capa', 'Capa do Serviço') !!}
    @if($submitText == 'Alterar')
    @if($servico->capa)
    <img src="{{ url('assets/img/servicos/'.$servico->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    @endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('titulo_pt', 'Título (PT)') !!}
        {!! Form::text('titulo_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('titulo_en', 'Título (EN)') !!}
        {!! Form::text('titulo_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('titulo_es', 'Título (ES)') !!}
        {!! Form::text('titulo_es', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('texto_pt', 'Texto Serviço (PT)') !!}
        {!! Form::textarea('texto_pt', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('texto_en', 'Texto Serviço (EN)') !!}
        {!! Form::textarea('texto_en', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('texto_es', 'Texto Serviço (ES)') !!}
        {!! Form::textarea('texto_es', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('grupos.servicos.index', $grupo->id) }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>