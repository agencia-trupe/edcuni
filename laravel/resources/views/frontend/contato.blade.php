<section class="fale-com" id="faleComUmEspecialista">
    <div class="dente-de-leao center"></div>
    <h4 class="titulo">{{ trans('frontend.geral.contato') }}</h4>
    <hr class="linha-titulo">
    @php $celular = "+55".str_replace(" ", "", $contato->celular); @endphp
    <a href="https://api.whatsapp.com/send?phone={{ $celular }}" class="link-whatsapp" target="_blank">
        <img src="{{ asset('assets/img/layout/ico-whatsapp-color.svg') }}" alt="" class="img-wpp">
        +55 {{ $contato->celular }}
    </a>
    <div class="form-contato">
        <img src="{{ asset('assets/img/contatos/'.$contato->imagem) }}" class="img-contato">
        <form action="{{ route('contato.post') }}" method="POST" enctype="multipart/form-data">
            {!! csrf_field() !!}
            <input type="text" name="nome" placeholder="{{ trans('frontend.contato.nome') }}" value="{{ old('nome') }}" required>
            <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
            <input type="text" name="telefone" placeholder="{{ trans('frontend.contato.telefone') }}" class="input-telefone" value="{{ old('telefone') }}">
            <input type="text" name="empresa" placeholder="{{ trans('frontend.contato.empresa') }}" value="{{ old('empresa') }}">
            <textarea name="mensagem" placeholder="{{ trans('frontend.contato.mensagem') }}" required>{{ old('mensagem') }}</textarea>
            <button type="submit" class="btn-enviar">
                {{ trans('frontend.contato.enviar') }}
                <img src="{{ asset('assets/img/layout/seta-enviar.svg') }}" alt="" class="img-enviar">
            </button>

            @if($errors->any())
            <div class="flash flash-erro">
                @foreach($errors->all() as $error)
                {!! $error !!}<br>
                @endforeach
            </div>
            @endif

            @if(session('enviado'))
            <div class="flash flash-sucesso">
                <p>{{ trans('frontend.contato.msg-sucesso') }}</p>
            </div>
            @endif
        </form>
    </div>
</section>