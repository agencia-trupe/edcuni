<?php

namespace App\Http\Controllers;

use App\Http\Requests\TrabalheConoscoRequest;
use App\Models\Contato;
use App\Models\TrabalheConosco;
use App\Notifications\TrabalheConoscoNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class TrabalheConoscoController extends Controller
{
    public function removerAcentos()
    {
        $conversao = array(
            'á' => 'a', 'à' => 'a', 'ã' => 'a', 'â' => 'a', 'é' => 'e',
            'ê' => 'e', 'í' => 'i', 'ï' => 'i', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', "ö" => "o",
            'ú' => 'u', 'ü' => 'u', 'ç' => 'c', 'ñ' => 'n', 'Á' => 'A', 'À' => 'A', 'Ã' => 'A',
            'Â' => 'A', 'É' => 'E', 'Ê' => 'E', 'Í' => 'I', 'Ï' => 'I', "Ö" => "O", 'Ó' => 'O',
            'Ô' => 'O', 'Õ' => 'O', 'Ú' => 'U', 'Ü' => 'U', 'Ç' => 'C', 'Ñ' => 'N'
        );

        return $conversao;
    }

    public function index()
    {
        return view('frontend.trabalhe-conosco');
    }

    public function post(TrabalheConoscoRequest $request)
    {
        try {
            $data = $request->all();

            $file = $request->file('arquivo');
            if ($request->hasFile('arquivo')) {
                if ($request->file('arquivo')->isValid()) {
                    $filename = strtoupper(str_replace(' ', '-', strtr($file->getClientOriginalName(), $this->removerAcentos())));
                    $path = public_path() . '/assets/arquivos/';
                    $file->move($path, $filename);
                    $data['arquivo'] = $filename;
                }
            }

            $contato = TrabalheConosco::create($data);

            Notification::send(
                Contato::first(),
                new TrabalheConoscoNotification($contato)
            );

            return back()->with('enviado', true);
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao enviar contato: ' . $e->getMessage()]);
        }
    }

    public function openArquivoPDF($id)
    {
        $arquivo = TrabalheConosco::where('id', $id)->select('arquivo')->first();
        $arquivoPath = public_path() . "/assets/arquivos/" . $arquivo->arquivo;

        return response()->file($arquivoPath);
    }
}
