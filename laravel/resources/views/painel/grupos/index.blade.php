@extends('painel.layout.template')

@section('content')

@include('painel.layout.flash')

<legend class="d-flex flex-row align-items-center justify-content-between mb-4">
    <h2 class="m-0">GRUPOS</h2>

    <a href="{{ route('grupos.create') }}" class="btn btn-success btn-sm">
        <i class="bi bi-plus-circle me-2 mb-1"></i>
        Adicionar Grupo
    </a>
</legend>


@if(!count($grupos))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="grupos">
        <thead>
            <tr>
                <th scope="col">Ordenar</th>
                <th scope="col">Ícone</th>
                <th scope="col">Titulo</th>
                <th scope="col">Gerenciar</th>
                <th class="no-filter" scope="col"><i class="bi bi-gear-fill me-2"></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($grupos as $grupo)
            <tr id="{{ $grupo->id }}">
                <td>
                    <a href="#" class="btn btn-dark btn-sm btn-move">
                        <i class="bi bi-arrows-move"></i>
                    </a>
                </td>
                <td>
                    <img src="{{ asset('assets/img/grupos/'.$grupo->icone) }}" style="width: auto; max-width:100px;" alt="">
                </td>
                <td>{{ $grupo->titulo_pt }}</td>
                <td>
                    <a href="{{ route('grupos.servicos.index', $grupo->id) }}" class="btn btn-secondary btn-gerenciar btn-sm">
                        <i class="bi bi-list me-2"></i>SERVIÇOS
                    </a>
                </td>
                <td class="crud-actions">
                    {!! Form::open([
                    'route' => ['grupos.destroy', $grupo->id],
                    'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm" role="group">
                        <a href="{{ route('grupos.edit', $grupo->id ) }}" class="btn btn-primary btn-sm">
                            <i class="bi bi-pencil-fill me-2"></i>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><i class="bi bi-trash-fill me-2"></i>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif

@endsection