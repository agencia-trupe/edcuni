<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\HomeRequest;
use App\Models\Home;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $home = Home::first();

        return view('painel.home.edit', compact('home'));
    }

    public function update(HomeRequest $request, Home $home)
    {
        try {
            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Home::upload_capa();
            if (isset($input['imagem_banner'])) $input['imagem_banner'] = Home::upload_imagem_banner();
            if (isset($input['imagem_edc'])) $input['imagem_edc'] = Home::upload_imagem_edc();
            if (isset($input['imagem_quantidades'])) $input['imagem_quantidades'] = Home::upload_imagem_quantidades();
            if (isset($input['imagem_quem_faz'])) $input['imagem_quem_faz'] = Home::upload_imagem_quem_faz();

            $home->update($input);

            return redirect()->route('home.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
