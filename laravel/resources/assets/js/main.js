import MobileToggle from "./MobileToggle";

MobileToggle();

$(document).ready(function () {
    // LANG
    $("select[name=lang]").change(function selectRoute() {
        window.location = $(this)
            .find(`option[value=${this.value}]`)
            .data("route");
    });

    // MASK telefone
    $(".input-telefone").mask("(00) 000000000");

    // Âncoras (header e footer) - QUEM FAZ
    $("header .link-nav.nav-quem-faz, footer .link-footer.nav-quem-faz").click(
        function (e) {
            e.preventDefault();
            var element = "#quemFaz";

            if (window.location.href == routeHome) {
                var targetOffset = $("#quemFaz").offset().top;
                $("html, body").animate({ scrollTop: targetOffset }, 1500);
            } else {
                window.location.href = routeHome + element;
            }
        }
    );

    // Âncoras (header e footer) - CONTATO
    $("header .link-nav.nav-contato, footer .link-footer.nav-contato").click(
        function (e) {
            e.preventDefault();
            var element = "#faleComUmEspecialista";

            if (window.location.pathname == "/trabalhe-conosco") {
                window.location.href = routeHome + element;
            } else {
                var targetOffset = $("#faleComUmEspecialista").offset().top;
                $("html, body").animate({ scrollTop: targetOffset }, 1500);
            }
        }
    );

    // Âncoras (header e footer) - NOSSOS SERVIÇOS
    $(
        "header .link-nav.nav-nossos-servicos, footer .link-footer.nav-nossos-servicos"
    ).click(function (e) {
        e.preventDefault();
        var element = "#nossosServicos";

        if (window.location.href == routeHome) {
            var targetOffset = $("#nossosServicos").offset().top;
            $("html, body").animate({ scrollTop: targetOffset }, 1500);
        } else {
            window.location.href = routeHome + element;
        }
    });

    // Whatsapp floating
    $("#chatWhatsapp").floatingWhatsApp({
        phone: "+55" + whatsappNumero,
        popupMessage: "Seja muito bem-vindo(a) à EDC UNI. Como podemos ajudar?",
        showPopup: true,
        autoOpen: false,
        headerTitle:
            '<div class="img-edc-uni"><img src="' +
            imgMarcaWhatsapp +
            '" alt=""></div>' +
            '<div class="textos"><p class="titulo">EDC UNI</p><p class="frase">Responderemos o mais breve possível.</p></div>',
        headerColor: "#25D366",
        size: "60px",
        position: "right",
        linkButton: true,
    });

    // Submenu PESSOAS e ESTRATÉGIA
    if ($(window).width() < 1200) {
        $("#navPessoas, #navEstrategia, .link-nav").click(function (e) {
            e.preventDefault();
            $(".submenu-nav").css("display", "none");
            $("header .link-nav.sub-active").removeClass("sub-active");
            var submenu = $(this).attr("submenu");
            $(this)
                .parent()
                .children("." + submenu)
                .css("display", "flex");
            $(this).addClass("sub-active");
        });
    } else {
        $("#navPessoas, #navEstrategia, .link-nav").mouseenter(function () {
            $(".submenu-nav").css("display", "none");
            $("header .link-nav.sub-active").removeClass("sub-active");
            var submenu = $(this).attr("submenu");
            $(this)
                .parent()
                .children("." + submenu)
                .css("display", "flex");
            $(this).addClass("sub-active");
        });
        $(".submenu-pessoas, .submenu-estrategia").mouseleave(function () {
            $(this).css("display", "none");
            $("header .link-nav.sub-active").removeClass("sub-active");
        });
    }

    // active PESSOAS e ESTRATÉGIA
    var itensPessoas = $(".submenu-pessoas .link-submenu");
    if (itensPessoas.hasClass("active")) {
        $(".link-nav.active").removeClass("active");
        $("#navPessoas").addClass("active");
    }
    var itensEstrategia = $(".submenu-estrategia .link-submenu");
    if (itensEstrategia.hasClass("active")) {
        $(".link-nav.active").removeClass("active");
        $("#navEstrategia").addClass("active");
    }

    // HOME - capas
    $(".capas")
        .on("cycle-initialized", function (e, o) {
            $(".capas").css("display", "block");
        })
        .cycle({
            slides: ".capa",
            fx: "fade",
            speed: 800,
            timeout: 4000,
        });

    // HOME - efeito quantidades
    if (window.location.href == routeHome) {
        $(window).on("load scroll", function () {
            var itensQuantidades = $("section.quantidades .item .quantidade");
            var executou = false;
            if (
                !executou &&
                $(window).scrollTop() > $("#homeQuantidades").offset().top / 2
            ) {
                console.log(itensQuantidades);
                itensQuantidades.animate(
                    { width: "80px", height: "80px" },
                    3000
                );
                executou = true;
            }
        });
    }

    // AVISO DE COOKIES
    $(".aviso-cookies").show();

    $(".aceitar-cookies").click(function () {
        var url = window.location.origin + "/aceite-de-cookies";

        $.ajax({
            type: "POST",
            url: url,
            success: function (data, textStatus, jqXHR) {
                $(".aviso-cookies").hide();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
            },
        });
    });
});
