<?php

namespace App\Models;

use App\Helpers\CropImage;
use App\Helpers\CropImageTinify;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Grupo extends Model
{
    use HasFactory;

    protected $table = 'grupos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_icone()
    {
        if (Configuracao::first()->tinify_key) {
            return CropImageTinify::make('icone', [
                'width'  => null,
                'height' => null,
                'transparent' => true,
                'path'   => 'assets/img/grupos/'
            ]);
        } else {
            return CropImage::make('icone', [
                'width'  => null,
                'height' => null,
                'transparent' => true,
                'path'   => 'assets/img/grupos/'
            ]);
        }
    }
}
