<?php

use App\Http\Controllers\GruposController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\TrabalheConoscoController;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$grupos = Lang::get('routes.grupos');

Route::get('/', [HomeController::class, 'index'])->name('home');

Route::get('en/groups/{slug}', [GruposController::class, 'index'])->name('grupos-en');
Route::get('es/grupos/{slug}', [GruposController::class, 'index'])->name('grupos-es');
Route::get('grupos/{slug}', [GruposController::class, 'index'])->name('grupos');

Route::post('contato', [HomeController::class, 'postContato'])->name('contato.post');

Route::get('en/work-with-us', [TrabalheConoscoController::class, 'index'])->name('trabalhe-conosco-en');
Route::get('es/trabaja-con-nosotros', [TrabalheConoscoController::class, 'index'])->name('trabalhe-conosco-es');
Route::get('trabalhe-conosco', [TrabalheConoscoController::class, 'index'])->name('trabalhe-conosco');
Route::post('trabalhe-conosco', [TrabalheConoscoController::class, 'post'])->name('trabalhe-conosco.post');
Route::get('trabalhe-conosco/{id}', [TrabalheConoscoController::class, 'openArquivoPDF'])->name('trabalhe-conosco.arquivo');

Route::post('aceite-de-cookies', [HomeController::class, 'postCookies'])->name('aceite-de-cookies.post');

// LANG
Route::get('lang/{lang}', function ($lang) {
    if (in_array($lang, ['pt', 'en', 'es'])) {
        Session::put('locale', $lang);
    }
    return back();
})->name('lang');

require __DIR__ . '/auth.php';
