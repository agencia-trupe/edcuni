<?php

namespace App\Models;

use App\Helpers\CropImage;
use App\Helpers\CropImageTinify;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Home extends Model
{
    use HasFactory;

    protected $table = 'home';

    protected $guarded = ['id'];

    public static function upload_imagem_banner()
    {
        if (Configuracao::first()->tinify_key) {
            return CropImageTinify::make('imagem_banner', [
                'width'  => 1980,
                'height' => null,
                'path'   => 'assets/img/home/'
            ]);
        } else {
            return CropImage::make('imagem_banner', [
                'width'  => 1980,
                'height' => null,
                'path'   => 'assets/img/home/'
            ]);
        }
    }

    public static function upload_imagem_edc()
    {
        if (Configuracao::first()->tinify_key) {
            return CropImageTinify::make('imagem_edc', [
                'width'  => 220,
                'height' => 220,
                'path'   => 'assets/img/home/'
            ]);
        } else {
            return CropImage::make('imagem_edc', [
                'width'  => 220,
                'height' => 220,
                'path'   => 'assets/img/home/'
            ]);
        }
    }

    public static function upload_imagem_quantidades()
    {
        if (Configuracao::first()->tinify_key) {
            return CropImageTinify::make('imagem_quantidades', [
                'width'  => 1980,
                'height' => null,
                'path'   => 'assets/img/home/'
            ]);
        } else {
            return CropImage::make('imagem_quantidades', [
                'width'  => 1980,
                'height' => null,
                'path'   => 'assets/img/home/'
            ]);
        }
    }

    public static function upload_imagem_quem_faz()
    {
        if (Configuracao::first()->tinify_key) {
            return CropImageTinify::make('imagem_quem_faz', [
                'width'  => 770,
                'height' => null,
                'path'   => 'assets/img/home/'
            ]);
        } else {
            return CropImage::make('imagem_quem_faz', [
                'width'  => 770,
                'height' => null,
                'path'   => 'assets/img/home/'
            ]);
        }
    }
}
