<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCapasTable extends Migration
{
    public function up()
    {
        Schema::create('capas', function (Blueprint $table) {
            $table->id();
            $table->integer('ordem')->default(0);
            $table->string('capa');
            $table->string('titulo_pt');
            $table->string('titulo_en')->nullable();
            $table->string('titulo_es')->nullable();
            $table->string('frase_pt');
            $table->string('frase_en')->nullable();
            $table->string('frase_es')->nullable();
            $table->string('link')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('capas');
    }
}
