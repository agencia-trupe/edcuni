<?php

return [
    '404' => 'Page not found',

    'geral' => [
        'vagas'             => 'Jobs',
        'vagas-texto'       => 'Work with us. Search available jobs',
        'area-do-candidato' => 'Candidate Area',
        'area-do-consultor' => 'Consultant Area',
        'termos'            => 'Terms of use',
        'politica'          => 'Privacy Policy',
        'direitos'          => 'All rights reserved',
        'criacao'           => 'Website creation: ',
        'edc-group'         => 'It is an EDC Group company',
        'nossos-servicos'   => 'OUR SERVICES',
        'pessoas'           => 'PEOPLE',
        'estrategia'        => 'STRATEGY',
        'quem-faz'          => 'WHO DOES',
        'contato'           => 'TALK TO AN EXPERT',
        'treinamentos'      => 'TRAININGS',
        'trabalhe-conosco'  => 'WORK WITH US',
        'selo-slac1'        => 'COACH ACKNOWLEDGED AND APPROVED',
        'selo-slac2'        => 'Latin American Coaching Society',
        'selo-iso1'         => 'Company with ',
        'selo-iso2'         => 'ISO 9001 CERTIFICATION',
        'selo-iso3'         => 'Project Engineering and Mechanics',
        'selo-iso4'         => 'Temporary Services and HR Management',
    ],

    'home' => [
        '+de'            => '+of',
        'frase-grupo'    => 'EDC SERVICES IS AN EDC GROUP COMPANY • DISCOVER THE GROUP COMPANIES:',
        'edc-servicos'   => 'EDC Services',
        'visite-website' => 'VISIT THE WEBSITE',
        'edc-engenharia' => 'EDC Engineering',
        'edc-uni'        => 'EDC UNI',
    ],

    'contato' => [
        'nome'        => 'name',
        'telefone'    => 'telephone',
        'empresa'     => 'company',
        'mensagem'    => 'message',
        'enviar'      => 'SEND',
        'msg-sucesso' => 'Message sent successfully!',
        'anexar'      => 'attach resume',
        'selecionar'  => 'SELECT FILE',
    ],

    'cookies1' => 'We use cookies to personalize content, track ads and provide a safer browsing experience for you. By continuing to browse our website, you agree to our use of this information. Read our ',
    'cookies2' => 'Privacy Policy',
    'cookies3' => ' and learn more.',
    'btn-cookies' => 'ACCEPT AND CLOSE',

];
