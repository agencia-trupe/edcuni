<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Models\TrabalheConosco;
use Illuminate\Http\Request;

class TrabalheConoscoController extends Controller
{
    public function index()
    {
        $curriculos = TrabalheConosco::orderBy('created_at', 'DESC')->get();

        return view('painel.contatos.curriculos.index', compact('curriculos'));
    }

    public function show(TrabalheConosco $trabalhe_conosco)
    {
        $trabalhe_conosco->update(['lido' => 1]);
        $curriculo = $trabalhe_conosco;

        return view('painel.contatos.curriculos.show', compact('curriculo'));
    }

    public function destroy(TrabalheConosco $trabalhe_conosco)
    {
        try {
            $trabalhe_conosco->delete();

            return redirect()->route('trabalhe-conosco.index')->with('success', 'Mensagem excluída com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir mensagem: ' . $e->getMessage()]);
        }
    }

    public function toggle($id)
    {
        try {
            $curriculo = TrabalheConosco::where('id', $id)->first();
            $curriculo->update([
                'lido' => !$curriculo->lido
            ]);

            return redirect()->route('trabalhe-conosco.index')->with('success', 'Mensagem alterada com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar mensagem: ' . $e->getMessage()]);
        }
    }
}
