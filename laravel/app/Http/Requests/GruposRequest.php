<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GruposRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'icone'         => 'image',
            'titulo_pt'     => 'required',
            'frase_pt'      => 'required',
            'texto_pt'      => 'required',
            'texto_home_pt' => 'required',
        ];
    }
}
