<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Models\ContatoRecebido;
use Illuminate\Http\Request;

class ContatosRecebidosController extends Controller
{
    public function index()
    {
        $contatos = ContatoRecebido::orderBy('created_at', 'DESC')->get();

        return view('painel.contatos.recebidos.index', compact('contatos'));
    }

    public function show($id)
    {
        $recebido = ContatoRecebido::where('id', $id)->first();
        $recebido->update(['lido' => 1]);

        return view('painel.contatos.recebidos.show', compact('recebido'));
    }

    public function destroy($id)
    {
        try {
            $recebido = ContatoRecebido::where('id', $id)->first();
            $recebido->delete();

            return redirect()->route('contatos-recebidos.index')->with('success', 'Mensagem excluída com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir mensagem: ' . $e->getMessage()]);
        }
    }

    public function toggle($id)
    {
        try {
            $recebido = ContatoRecebido::where('id', $id)->first();
            $recebido->update([
                'lido' => !$recebido->lido
            ]);

            return redirect()->route('contatos-recebidos.index')->with('success', 'Mensagem alterada com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar mensagem: ' . $e->getMessage()]);
        }
    }
}
