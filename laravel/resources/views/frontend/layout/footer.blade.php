<footer>
    <div class="center">
        <article class="marcas">
            <a href="{{ route('home') }}" class="link-home">
                <img src="{{ asset('assets/img/layout/marca-edcuni.svg') }}" alt="{{ $config->{trans('database.title')} }}" class="img-logo">
            </a>
            <a href="https://www.edcgroup.com.br/" target="_blank" class="link-edc-group">
                <p class="frase">{{ trans('frontend.geral.edc-group') }}</p>
                <img src="{{ asset('assets/img/layout/marca-edc-group-branco.svg') }}" alt="EDC GROUP" class="img-edc-group">
            </a>
        </article>

        <article class="contatos">
            <div class="redes-sociais">
                <a href="{{ $contato->facebook }}" target="_blank" class="facebook" title="Facebook"><img src="{{ asset('assets/img/layout/ico-facebook.svg') }}" alt=""></a>
                <a href="{{ $contato->linkedin }}" target="_blank" class="linkedin" title="LinkedIn"><img src="{{ asset('assets/img/layout/ico-linkedin.svg') }}" alt=""></a>
                <a href="{{ $contato->instagram }}" target="_blank" class="instagram" title="Instagram"><img src="{{ asset('assets/img/layout/ico-instagram-branco.svg') }}" alt=""></a>
            </div>
            <div class="informacoes">
                @php
                $telefone = "+55".str_replace(" ", "", $contato->telefone);
                $celular = "+55".str_replace(" ", "", $contato->celular);
                @endphp
                <a href="tel:+55{{ $contato->telefone }}" class="link-telefone">+55 {{ $contato->telefone }}</a>
                <a href="https://api.whatsapp.com/send?phone={{ $celular }}" class="link-wpp" target="_blank">+55 {{ $contato->celular }}</a>
                <a href="mailto:{{ $contato->email }}" class="link-email">{{ $contato->email }}</a>
            </div>
            <p class="endereco">{{ $contato->{trans('database.endereco_pt1')} }}</p>
            <p class="endereco">{{ $contato->{trans('database.endereco_pt2')} }}</p>
        </article>

        <article class="nav-footer">
            <a href="{{ route('home') }}" class="link-footer @if(Tools::routeIs('home')) active @endif">
                <span>•</span>HOME
            </a>
            <a href="" class="link-footer nav-nossos-servicos">
                <span>•</span>{{ trans('frontend.geral.nossos-servicos') }}
            </a>
            <a href="" class="link-footer nav-quem-faz">
                <span>•</span>{{ trans('frontend.geral.quem-faz') }}
            </a>
            <a href="" class="link-footer nav-contato">
                <span>•</span>{{ trans('frontend.geral.contato') }}
            </a>
            <a href="https://www.edcgroup.com.br/termos-de-uso" class="link-termos" target="_blank">
                » {{ trans('frontend.geral.termos') }}
            </a>

            <a href="https://www.edcgroup.com.br/politica-de-privacidade" class="link-politica" target="_blank">
                » {{ trans('frontend.geral.politica') }}
            </a>
        </article>

        <article class="selos">
            <div class="slac">
                <img src="{{ asset('assets/img/layout/selo-slac.png') }}" alt="SLAC" class="img-selo">
                <div class="textos">
                    <p class="frase">{{ trans('frontend.geral.selo-slac1') }}</p>
                    <p class="frase"><strong>SLAC </strong>{{ trans('frontend.geral.selo-slac2') }}</p>
                    <a href="https://www.slacoaching.com.br/" target="_blank" class="link-slac">www.slacoaching.com.br</a>
                </div>
            </div>
            {{-- <div class="iso">
                <img src="{{ asset('assets/img/layout/selo-iso.png') }}" alt="ISO 9001" class="img-selo">
                <div class="textos">
                    <p class="frase">{{ trans('frontend.geral.selo-iso1') }} <strong> {{ trans('frontend.geral.selo-iso2') }}</strong></p>
                    <p class="frase">{{ trans('frontend.geral.selo-iso3') }}</p>
                    <p class="frase">{{ trans('frontend.geral.selo-iso4') }}</p>
                </div>
            </div> --}}
        </article>
    </div>
    <article class="copyright">
        <p class="direitos"> © {{ date('Y') }} {{ config('app.name') }} - {{ trans('frontend.geral.direitos') }}</p>
        <span>|</span>
        <a href="https://www.trupe.net" target="_blank" class="link-trupe">{{ trans('frontend.geral.criacao') }}</a>
        <a href="https://www.trupe.net" target="_blank" class="link-trupe">Trupe Agência Criativa</a>
    </article>
</footer>