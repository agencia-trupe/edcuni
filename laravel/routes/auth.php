<?php

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\ConfirmablePasswordController;
use App\Http\Controllers\Auth\NewPasswordController;
use App\Http\Controllers\Auth\PasswordResetLinkController;
use App\Http\Controllers\Painel\AceiteDeCookiesController;
use App\Http\Controllers\Painel\CapasController;
use App\Http\Controllers\Painel\ConfiguracoesController;
use App\Http\Controllers\Painel\ContatosController;
use App\Http\Controllers\Painel\ContatosRecebidosController;
use App\Http\Controllers\Painel\EmpresasGrupoController;
use App\Http\Controllers\Painel\GruposController;
use App\Http\Controllers\Painel\HomeController;
use App\Http\Controllers\Painel\PainelController;
use App\Http\Controllers\Painel\ServicosController;
use App\Http\Controllers\Painel\TrabalheConoscoController;
use App\Http\Controllers\Painel\UsersController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => 'painel',
], function () {
    // AUTH - BREEZE
    Route::get('/login', [AuthenticatedSessionController::class, 'create'])
        ->middleware('guest')
        ->name('login');
    Route::post('/login', [AuthenticatedSessionController::class, 'store'])
        ->middleware('guest');
    Route::get('/forgot-password', [PasswordResetLinkController::class, 'create'])
        ->middleware('guest')
        ->name('password.request');
    Route::post('/forgot-password', [PasswordResetLinkController::class, 'store'])
        ->middleware('guest')
        ->name('password.email');
    Route::get('/reset-password/{token}', [NewPasswordController::class, 'create'])
        ->middleware('guest')
        ->name('password.reset');
    Route::post('/reset-password', [NewPasswordController::class, 'store'])
        ->middleware('guest')
        ->name('password.update');
    Route::get('/confirm-password', [ConfirmablePasswordController::class, 'show'])
        ->middleware('auth')
        ->name('password.confirm');
    Route::post('/confirm-password', [ConfirmablePasswordController::class, 'store'])
        ->middleware('auth');
    Route::post('/logout', [AuthenticatedSessionController::class, 'destroy'])
        ->middleware('auth')
        ->name('logout');

    // PAINEL
    Route::group([
        'middleware' => ['auth']
    ], function () {
        Route::get('/', [PainelController::class, 'index'])->name('painel');
        Route::post('image-upload', [PainelController::class, 'imageUpload']);
        Route::post('order', [PainelController::class, 'order']);
        Route::resource('usuarios', UsersController::class);
        Route::resource('configuracoes', ConfiguracoesController::class)->only(['index', 'update']);
        Route::get('aceite-de-cookies', [AceiteDeCookiesController::class, 'index'])->name('aceite-de-cookies.index');
        Route::resource('contatos', ContatosController::class)->only(['index', 'update']);
        Route::resource('home', HomeController::class)->only(['index', 'update']);
        Route::resource('capas', CapasController::class);
        Route::resource('grupos', GruposController::class);
        Route::resource('grupos.servicos', ServicosController::class);
        Route::resource('empresas-grupo', EmpresasGrupoController::class)->only(['index', 'edit', 'update']);
        Route::get('contatos-recebidos/{id}/toggle', [ContatosRecebidosController::class, 'toggle'])->name('contatos-recebidos.toggle');
        Route::resource('contatos-recebidos', ContatosRecebidosController::class)->only(['index', 'show', 'destroy']);
        Route::get('trabalhe-conosco/{id}/toggle', [TrabalheConoscoController::class, 'toggle'])->name('trabalhe-conosco.toggle');
        Route::resource('trabalhe-conosco', TrabalheConoscoController::class)->only(['index', 'show', 'destroy']);

        // Limpar caches
        Route::get('clear-cache', function () {
            $exitCode = Artisan::call('config:clear');
            $exitCode = Artisan::call('route:clear');
            $exitCode = Artisan::call('cache:clear');

            return 'DONE';
        });
    });
});
