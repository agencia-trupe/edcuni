<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\GruposRequest;
use App\Models\Grupo;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class GruposController extends Controller
{
    public function index()
    {
        $grupos = Grupo::ordenados()->get();

        return view('painel.grupos.index', compact('grupos'));
    }

    public function create()
    {
        return view('painel.grupos.create');
    }

    public function store(GruposRequest $request)
    {
        try {
            $input = $request->all();

            $input['slug_pt'] = Str::slug($request->titulo_pt, "-");
            if (isset($input['titulo_en'])) {
                $input['slug_en'] = Str::slug($request->titulo_en, "-");
            } else {
                $input['slug_en'] = Str::slug($request->titulo_pt, "-");
            }

            if (isset($input['titulo_es'])) {
                $input['slug_es'] = Str::slug($request->titulo_es, "-");
            } else {
                $input['slug_es'] = Str::slug($request->titulo_pt, "-");
            }

            if (isset($input['icone'])) $input['icone'] = Grupo::upload_icone();

            Grupo::create($input);

            return redirect()->route('grupos.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Grupo $grupo)
    {
        return view('painel.grupos.edit', compact('grupo'));
    }

    public function update(GruposRequest $request, Grupo $grupo)
    {
        try {
            $input = $request->all();

            $input['slug_pt'] = Str::slug($request->titulo_pt, "-");
            if (isset($input['titulo_en'])) $input['slug_en'] = Str::slug($request->titulo_en, "-");
            if (isset($input['titulo_es'])) $input['slug_es'] = Str::slug($request->titulo_es, "-");

            if (isset($input['icone'])) $input['icone'] = Grupo::upload_icone();

            $grupo->update($input);

            return redirect()->route('grupos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Grupo $grupo)
    {
        try {
            $grupo->delete();

            return redirect()->route('grupos.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
