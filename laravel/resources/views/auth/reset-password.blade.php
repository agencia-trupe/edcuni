<!doctype html>
<html lang="pt-BR">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name') }} - Painel Administrativo</title>

    <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/painel.css') }}">
</head>

<body class="painel-login">

    <div class="col-md-4 col-sm-6 col-xs-10 login">

        <h3 class="mb-5">
            {{ config('app.name') }}
            <small class="h6 lead mb-0" style="color:#b4bcc2;">Painel Administrativo</small>
        </h3>

        {!! Form::open(['route' => 'password.update']) !!}

        <!-- Password Reset Token -->
        <input type="hidden" name="token" value="{{ $request->route('token') }}">

        <!-- Email Address -->
        <div class="input-group">
            <span class="input-group-text">E-MAIL:</span>
            {!! Form::text('email', null, [
            'class' => 'form-control',
            'placeholder' => 'usuário ou e-mail',
            'autofocus' => true,
            'required' => true
            ]) !!}
        </div>

        <!-- Password -->
        <div class="input-group">
            <span class="input-group-text">NOVA SENHA:</span>
            {!! Form::password('password', [
            'class' => 'form-control',
            'required' => true
            ]) !!}
        </div>

        <!-- Confirm Password -->
        <div class="input-group">
            <span class="input-group-text">REPETIR SENHA:</span>
            {!! Form::password('password_confirmation', [
            'class' => 'form-control',
            'required' => true
            ]) !!}
        </div>

        {!! Form::submit('REDEFINIR SENHA', ['class' => 'btn btn-success col-12 mt-4', 'style' => 'height:45px']) !!}
        {!! Form::close() !!}

        <!-- Validation Errors -->
        <x-auth-validation-errors class="flash flash-erro mt-2" :errors="$errors" />
    </div>

</body>