<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContatosRecebidosRequest;
use App\Models\AceiteDeCookies;
use App\Models\Capa;
use App\Models\Contato;
use App\Models\ContatoRecebido;
use App\Models\Grupo;
use App\Models\Home;
use App\Notifications\ContatosRecebidosNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class HomeController extends Controller
{
    public function index()
    {
        $home = Home::first();
        $capas = Capa::ordenados()->get();
        $grupos = Grupo::ordenados()->get();

        return view('frontend.home', compact('home', 'capas', 'grupos'));
    }

    public function postContato(ContatosRecebidosRequest $request, ContatoRecebido $contato_recebido)
    {
        try {
            $data = $request->all();

            $contato_recebido->create($data);

            Notification::send(
                Contato::first(),
                new ContatosRecebidosNotification($data)
            );

            return back()->with('enviado', true);
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao enviar contato: ' . $e->getMessage()]);
        }
    }

    public function postCookies(Request $request)
    {
        $input = $request->all();
        $input['ip'] = $request->ip();

        AceiteDeCookies::create($input);

        return redirect('/');
    }
}
