<?php

return [
    '404' => 'Página não encontrada',

    'geral' => [
        'vagas'             => 'Vagas',
        'vagas-texto'       => 'Venha trabalhar com a gente. Confira nossas vagas',
        'area-do-candidato' => 'Área do Candidato',
        'area-do-consultor' => 'Área do Consultor',
        'termos'            => 'Termos de Uso',
        'politica'          => 'Política de Privacidade',
        'direitos'          => 'Todos os direitos reservados',
        'criacao'           => 'Criação de sites: ',
        'edc-group'         => 'É uma empresa do Grupo EDC',
        'nossos-servicos'   => 'NUESTROS SERVICIOS',
        'pessoas'           => 'GENTE',
        'estrategia'        => 'ESTRATEGIA',
        'quem-faz'          => 'QUIEN HACE',
        'contato'           => 'HABLAR CON UN EXPERTO',
        'treinamentos'      => 'ENTRENAMIENTOS',
        'trabalhe-conosco'  => 'TRABAJA CON NOSOTROS',
        'selo-slac1'        => 'ENTRENADOR RECONOCIDO Y APROBADO',
        'selo-slac2'        => 'Sociedad Latinoamericana de Coaching',
        'selo-iso1'         => 'Compañía con',
        'selo-iso2'         => 'CERTIFICACIÓN ISO 9001',
        'selo-iso3'         => 'Ingeniería y Mecánica de Proyectos',
        'selo-iso4'         => 'Servicios temporales y gestión de recursos humanos',
    ],

    'home' => [
        '+de'            => '+de',
        'frase-grupo'    => 'A EDC SERVIÇOS É UMA EMPRESA DA EDC GROUP • CONHEÇA AS EMPRESAS DO GRUPO:',
        'edc-servicos'   => 'EDC Serviços',
        'visite-website' => 'VISITE O WEBSITE',
        'edc-engenharia' => 'EDC Engenharia',
        'edc-uni'        => 'EDC UNI',
    ],

    'contato' => [
        'nome'        => 'nombre',
        'telefone'    => 'teléfono',
        'empresa'     => 'empresa',
        'mensagem'    => 'mensaje',
        'enviar'      => 'MANDAR',
        'msg-sucesso' => 'Mensaje enviado correctamente!',
        'anexar'      => 'adjuntar curriculum vitae',
        'selecionar'  => 'SELECCIONE ARCHIVO',
    ],

    'cookies1' => 'Usamos cookies para personalizar contenido, rastrear anuncios y brindarle una experiencia de navegación más segura. Si continúa navegando en nuestro sitio web, acepta nuestro uso de esta información. Lee nuestro ',
    'cookies2' => 'Política de privacidad',
    'cookies3' => ' y aprenda más.',
    'btn-cookies' => 'ACEPTAR Y CERRAR',

];
