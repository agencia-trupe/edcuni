<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmpresasGrupoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'logo' => 'image',
            'nome_pt' => 'required',
            'nome_en' => 'required',
            'nome_es' => 'required',
            'servicos_pt' => 'required',
            'servicos_en' => 'required',
            'servicos_es' => 'required',
        ];
    }
}
