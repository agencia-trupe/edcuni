@extends('frontend.layout.template')

@section('content')

<main class="home">

    <section class="capas center" style="display: none;">
        @foreach($capas as $capa)
        @if($capa->link)
        <a href="{{ $capa->link }}" class="capa" target="_blank">
            <h1 class="titulo-capa">{{ $capa->{trans('database.titulo')} }}</h1>
            <p class="frase-capa">{{ $capa->{trans('database.frase')} }}</p>
            <div class="bola">
                <img src="{{ asset('assets/img/home/capas/'.$capa->capa) }}" class="img-capa">
            </div>
        </a>
        @else
        <div class="capa">
            <h1 class="titulo-capa">{{ $capa->{trans('database.titulo')} }}</h1>
            <p class="frase-capa">{{ $capa->{trans('database.frase')} }}</p>
            <div class="bola">
                <img src="{{ asset('assets/img/home/capas/'.$capa->capa) }}" class="img-capa">
            </div>
        </div>
        @endif
        @endforeach
    </section>

    <section class="banner" style="background-image: url({{ asset('assets/img/home/'.$home->imagem_banner) }})">
        <div class="center">
            <h2 class="titulo-banner">{{ $home->{trans('database.titulo_banner')} }}</h2>
            <p class="frase-banner">{{ $home->{trans('database.frase_banner')} }}</p>
            <div class="btns-banner">
                @if(Lang::getLocale() == "en")
                <a href="{{ route('grupos-en', $grupo1->slug_en) }}" class="link-servico">
                    {{ $home->{trans('database.btn1_titulo')} }}
                </a>
                <a href="{{ route('grupos-en', $grupo2->slug_en) }}" class="link-servico">
                    {{ $home->{trans('database.btn2_titulo')} }}
                </a>
                @elseif(Lang::getLocale() == "es")
                <a href="{{ route('grupos-es', $grupo1->slug_es) }}" class="link-servico">
                    {{ $home->{trans('database.btn1_titulo')} }}
                </a>
                <a href="{{ route('grupos-es', $grupo2->slug_es) }}" class="link-servico">
                    {{ $home->{trans('database.btn2_titulo')} }}
                </a>
                @else
                <a href="{{ route('grupos', $grupo1->slug_pt) }}" class="link-servico">
                    {{ $home->{trans('database.btn1_titulo')} }}
                </a>
                <a href="{{ route('grupos', $grupo2->slug_pt) }}" class="link-servico">
                    {{ $home->{trans('database.btn2_titulo')} }}
                </a>
                @endif
            </div>
        </div>
    </section>

    <section class="proposito center">
        <div class="texto">{!! $home->{trans('database.texto_proposito')} !!}</div>
    </section>

    <section class="quantidades" style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, 0), rgba(117, 132, 111, 1)), url({{ asset('assets/img/home/'.$home->imagem_quantidades) }})" id="homeQuantidades">
        <div class="center">
            <article class="item">
                <div class="quantidade" style="width:0; height:0;">
                    <p class="mais-de">{{ trans('frontend.home.+de') }}</p>
                    <p class="numero">{{ $home->item1_quantidade }}</p>
                </div>
                <p class="nome-item">{{ $home->{trans('database.item1_titulo')} }}</p>
            </article>
            <article class="item">
                <div class="quantidade" style="width:0; height:0;">
                    <p class="mais-de">{{ trans('frontend.home.+de') }}</p>
                    <p class="numero">{{ $home->item2_quantidade }}</p>
                </div>
                <p class="nome-item c-level">{{ $home->{trans('database.item2_titulo')} }}</p>
            </article>
            <article class="item">
                <div class="quantidade" style="width:0; height:0;">
                    <p class="mais-de">{{ trans('frontend.home.+de') }}</p>
                    <p class="numero">{{ $home->item3_quantidade }}</p>
                </div>
                <p class="nome-item">{{ $home->{trans('database.item3_titulo')} }}</p>
            </article>
            <article class="item">
                <div class="quantidade" style="width:0; height:0;">
                    <p class="mais-de">{{ trans('frontend.home.+de') }}</p>
                    <p class="numero">{{ $home->item4_quantidade }}</p>
                </div>
                <p class="nome-item">{{ $home->{trans('database.item4_titulo')} }}</p>
            </article>
        </div>
    </section>

    <section class="edcgroup">
        <article class="imagens">
            <img src="{{ asset('assets/img/layout/globo-terrestre.png') }}" alt="" class="img-globo">
            <div class="imagem">
                <img src="{{ asset('assets/img/home/'.$home->imagem_edc) }}" class="img-edc">
            </div>
        </article>
        <article class="textos">
            <h4 class="titulo-edc">{{ $home->{trans('database.titulo_edc')} }}</h4>
            <hr class="linha-titulo">
            <div class="left">
                {!! $home->{trans('database.texto1_edc')} !!}
                <img src="{{ asset('assets/img/layout/marca-edc-group-branco.svg') }}" alt="" class="img-group">
            </div>
            <div class="right">
                {!! $home->{trans('database.texto2_edc')} !!}
            </div>
        </article>
    </section>

    <section class="grupos center" id="nossosServicos">
        <h4 class="frase-servicos">{{ $home->{trans('database.frase_servicos')} }}</h4>
        <hr class="linha-titulo">
        <h2 class="titulo-servicos">{{ $home->{trans('database.titulo_servicos')} }}</h2>
        <article class="itens-grupos">
            @foreach($grupos as $grupo)
            @if(Lang::getLocale() == "en")
            <a href="{{ route('grupos-en', $grupo->slug_en) }}" class="link-grupo">
                <div class="icone">
                    <img src="{{ asset('assets/img/grupos/'.$grupo->icone) }}" class="img-icone">
                </div>
                <h4 class="titulo-grupo">{{ $grupo->{trans('database.titulo')} }}</h4>
                <div class="texto-grupo">{!! $grupo->{trans('database.texto_home')} !!}</div>
            </a>
            @elseif(Lang::getLocale() == "es")
            <a href="{{ route('grupos-es', $grupo->slug_es) }}" class="link-grupo">
                <div class="icone">
                    <img src="{{ asset('assets/img/grupos/'.$grupo->icone) }}" class="img-icone">
                </div>
                <h4 class="titulo-grupo">{{ $grupo->{trans('database.titulo')} }}</h4>
                <div class="texto-grupo">{!! $grupo->{trans('database.texto_home')} !!}</div>
            </a>
            @else
            <a href="{{ route('grupos', $grupo->slug_pt) }}" class="link-grupo">
                <div class="icone">
                    <img src="{{ asset('assets/img/grupos/'.$grupo->icone) }}" class="img-icone">
                </div>
                <h4 class="titulo-grupo">{{ $grupo->{trans('database.titulo')} }}</h4>
                <div class="texto-grupo">{!! $grupo->{trans('database.texto_home')} !!}</div>
            </a>
            @endif
            @endforeach
        </article>
    </section>

    <section class="quem-faz" id="quemFaz">
        <h4 class="titulo">{{ trans('frontend.geral.quem-faz') }}</h4>
        <hr class="linha-titulo">
        <h2 class="titulo-quem-faz">{{ $home->{trans('database.titulo_quem_faz')} }}</h2>
        <p class="frase-quem-faz">{{ $home->{trans('database.frase_quem_faz')} }}</p>
        <article class="infos">
            <img src="{{ asset('assets/img/home/'.$home->imagem_quem_faz) }}" class="img-quem-faz">
            <div class="textos">
                <p class="nome">{{ $home->nome_quem_faz }}</p>
                <p class="cargo">{{ $home->{trans('database.titulo_cargo')} }}</p>
                <div class="texto">{!! $home->{trans('database.texto_quem_faz')} !!}</div>
            </div>
        </article>
    </section>

    @include('frontend.contato')

    @include('frontend.grupo-edc')

</main>

@endsection