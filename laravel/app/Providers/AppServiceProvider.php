<?php

namespace App\Providers;

use App\Models\AceiteDeCookies;
use App\Models\Configuracao;
use App\Models\Contato;
use App\Models\ContatoRecebido;
use App\Models\EmpresaGrupo;
use App\Models\Grupo;
use App\Models\Home;
use App\Models\TrabalheConosco;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('*', function ($view) {
            $view->with('config', Configuracao::first());
        });

        View::composer('frontend.grupos', function ($view) {
            $view->with('grupos', Grupo::ordenados()->get());
        });

        View::composer('frontend.*', function ($view) {
            $view->with('contato', Contato::first());

            $request = app(\Illuminate\Http\Request::class);
            $view->with('verificacao', AceiteDeCookies::where('ip', $request->ip())->first());

            $view->with('empresas', EmpresaGrupo::ordenados()->get());
        });

        View::composer('frontend.layout*', function ($view) {
            $gruposPessoas = [];
            $gruposPessoas[] = Grupo::where('slug_pt', 'LIKE', '%recrutamento%')->first();
            $gruposPessoas[] = Grupo::where('slug_pt', 'LIKE', '%capacitacao%')->first();
            $gruposPessoas[] = Grupo::where('slug_pt', 'LIKE', '%transformacao%')->first();
            $view->with('gruposPessoas', $gruposPessoas);

            $gruposEstrategia = [];
            $gruposEstrategia[] = Grupo::where('slug_pt', 'LIKE', '%data-analise%')->first();
            $gruposEstrategia[] = Grupo::where('slug_pt', 'LIKE', '%planejamento%')->first();
            $gruposEstrategia[] = Grupo::where('slug_pt', 'LIKE', '%gestao%')->first();
            $view->with('gruposEstrategia', $gruposEstrategia);
        });

        View::composer('frontend.home', function ($view) {
            $parts1 = parse_url(Home::first()->btn1_link);
            $path_parts = explode('/', $parts1['path']);
            $slug1 = $path_parts[2];
            $grupo1 = Grupo::where('slug_pt', $slug1)->first();

            $parts2 = parse_url(Home::first()->btn2_link);
            $path_parts = explode('/', $parts2['path']);
            $slug2 = $path_parts[2];
            $grupo2 = Grupo::where('slug_pt', $slug2)->first();

            $view->with('grupo1', $grupo1);
            $view->with('grupo2', $grupo2);
        });

        View::composer('painel.layout.*', function ($view) {
            $view->with('contatosNaoLidos', ContatoRecebido::naoLidos()->count());
            $view->with('curriculosNaoLidos', TrabalheConosco::naoLidos()->count());
        });
    }
}
