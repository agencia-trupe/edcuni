@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>CAPAS |</small> Adicionar Capa</h2>
</legend>

{!! Form::open(['route' => 'capas.store', 'files' => true]) !!}

@include('painel.home.capas.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection