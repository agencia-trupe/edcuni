@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>GRUPO: {{ $grupo->titulo_pt }} |</small> Adicionar Serviço</h2>
</legend>

{!! Form::open(['route' => ['grupos.servicos.store', $grupo->id], 'files' => true]) !!}

@include('painel.grupos.servicos.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection