@include('painel.layout.flash')

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('imagem_banner', 'Imagem de Fundo - Serviços') !!}
    @if($home->imagem_banner)
    <img src="{{ url('assets/img/home/'.$home->imagem_banner) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem_banner', ['class' => 'form-control']) !!}
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('titulo_banner_pt', 'Banner Home - Título (PT)') !!}
        {!! Form::text('titulo_banner_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('titulo_banner_en', 'Banner Home - Título (EN)') !!}
        {!! Form::text('titulo_banner_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('titulo_banner_es', 'Banner Home - Título (ES)') !!}
        {!! Form::text('titulo_banner_es', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase_banner_pt', 'Banner Home - Frase (PT)') !!}
        {!! Form::text('frase_banner_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase_banner_en', 'Banner Home - Frase (EN)') !!}
        {!! Form::text('frase_banner_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase_banner_es', 'Banner Home - Frase (ES)') !!}
        {!! Form::text('frase_banner_es', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="mb-3 col-12">
    {!! Form::label('btn1_link', 'Link Botão 1') !!}
    {!! Form::text('btn1_link', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('btn1_titulo_pt', 'Título Botão 1 (PT)') !!}
        {!! Form::text('btn1_titulo_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('btn1_titulo_en', 'Título Botão 1 (EN)') !!}
        {!! Form::text('btn1_titulo_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('btn1_titulo_es', 'Título Botão 1 (ES)') !!}
        {!! Form::text('btn1_titulo_es', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="mb-3 col-12">
    {!! Form::label('btn2_link', 'Link Botão 2') !!}
    {!! Form::text('btn2_link', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('btn2_titulo_pt', 'Título Botão 2 (PT)') !!}
        {!! Form::text('btn2_titulo_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('btn2_titulo_en', 'Título Botão 2 (EN)') !!}
        {!! Form::text('btn2_titulo_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('btn2_titulo_es', 'Título Botão 2 (ES)') !!}
        {!! Form::text('btn2_titulo_es', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<hr>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('texto_proposito_pt', 'Texto - Propósito - após 1º banner (PT)') !!}
        {!! Form::textarea('texto_proposito_pt', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('texto_proposito_en', 'Texto - Propósito - após 1º banner (EN)') !!}
        {!! Form::textarea('texto_proposito_en', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('texto_proposito_es', 'Texto - Propósito - após 1º banner (ES)') !!}
        {!! Form::textarea('texto_proposito_es', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
</div>

<hr>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('imagem_edc', 'Imagem EDC (área sobre EDC Group - ao lado do globo)') !!}
    @if($home->imagem_edc)
    <img src="{{ url('assets/img/home/'.$home->imagem_edc) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem_edc', ['class' => 'form-control']) !!}
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('titulo_edc_pt', 'Título (área sobre EDC Group) (PT)') !!}
        {!! Form::text('titulo_edc_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('titulo_edc_en', 'Título (área sobre EDC Group) (EN)') !!}
        {!! Form::text('titulo_edc_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('titulo_edc_es', 'Título (área sobre EDC Group) (ES)') !!}
        {!! Form::text('titulo_edc_es', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('texto1_edc_pt', 'Texto 1 (área sobre EDC Group) (PT)') !!}
        {!! Form::textarea('texto1_edc_pt', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('texto1_edc_en', 'Texto 1 (área sobre EDC Group) (EN)') !!}
        {!! Form::textarea('texto1_edc_en', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('texto1_edc_es', 'Texto 1 (área sobre EDC Group) (ES)') !!}
        {!! Form::textarea('texto1_edc_es', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('texto2_edc_pt', 'Texto 2 (área sobre EDC Group) (PT)') !!}
        {!! Form::textarea('texto2_edc_pt', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('texto2_edc_en', 'Texto 2 (área sobre EDC Group) (EN)') !!}
        {!! Form::textarea('texto2_edc_en', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('texto2_edc_es', 'Texto 2 (área sobre EDC Group) (ES)') !!}
        {!! Form::textarea('texto2_edc_es', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
</div>

<hr>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('imagem_quantidades', 'Imagem de Fundo - Quantidades') !!}
    @if($home->imagem_quantidades)
    <img src="{{ url('assets/img/home/'.$home->imagem_quantidades) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem_quantidades', ['class' => 'form-control']) !!}
</div>

<div class="row mb-2">
    <div class="mb-3 col-6 col-md-3">
        {!! Form::label('item1_titulo_pt', 'Título Item 1 (PT)') !!}
        {!! Form::text('item1_titulo_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-6 col-md-3">
        {!! Form::label('item1_titulo_en', 'Título Item 1 (EN)') !!}
        {!! Form::text('item1_titulo_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-6 col-md-3">
        {!! Form::label('item1_titulo_es', 'Título Item 1 (ES)') !!}
        {!! Form::text('item1_titulo_es', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-6 col-md-3">
        {!! Form::label('item1_quantidade', 'Quantidade 1 (+ de)') !!}
        {!! Form::number('item1_quantidade', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-6 col-md-3">
        {!! Form::label('item2_titulo_pt', 'Título Item 2 (PT)') !!}
        {!! Form::text('item2_titulo_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-6 col-md-3">
        {!! Form::label('item2_titulo_en', 'Título Item 2 (EN)') !!}
        {!! Form::text('item2_titulo_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-6 col-md-3">
        {!! Form::label('item2_titulo_es', 'Título Item 2 (ES)') !!}
        {!! Form::text('item2_titulo_es', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-6 col-md-3">
        {!! Form::label('item2_quantidade', 'Quantidade 2 (+ de)') !!}
        {!! Form::number('item2_quantidade', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-6 col-md-3">
        {!! Form::label('item3_titulo_pt', 'Título Item 3 (PT)') !!}
        {!! Form::text('item3_titulo_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-6 col-md-3">
        {!! Form::label('item3_titulo_en', 'Título Item 3 (EN)') !!}
        {!! Form::text('item3_titulo_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-6 col-md-3">
        {!! Form::label('item3_titulo_es', 'Título Item 3 (ES)') !!}
        {!! Form::text('item3_titulo_es', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-6 col-md-3">
        {!! Form::label('item3_quantidade', 'Quantidade 3 (+ de)') !!}
        {!! Form::number('item3_quantidade', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-6 col-md-3">
        {!! Form::label('item4_titulo_pt', 'Título Item 4 (PT)') !!}
        {!! Form::text('item4_titulo_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-6 col-md-3">
        {!! Form::label('item4_titulo_en', 'Título Item 4 (EN)') !!}
        {!! Form::text('item4_titulo_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-6 col-md-3">
        {!! Form::label('item4_titulo_es', 'Título Item 4 (ES)') !!}
        {!! Form::text('item4_titulo_es', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-6 col-md-3">
        {!! Form::label('item4_quantidade', 'Quantidade 4 (+ de)') !!}
        {!! Form::number('item4_quantidade', null, ['class' => 'form-control']) !!}
    </div>
</div>

<hr>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('titulo_servicos_pt', 'Título Serviços (PT)') !!}
        {!! Form::text('titulo_servicos_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('titulo_servicos_en', 'Título Serviços (EN)') !!}
        {!! Form::text('titulo_servicos_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('titulo_servicos_es', 'Título Serviços (ES)') !!}
        {!! Form::text('titulo_servicos_es', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase_servicos_pt', 'Frase Serviços (PT)') !!}
        {!! Form::text('frase_servicos_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase_servicos_en', 'Frase Serviços (EN)') !!}
        {!! Form::text('frase_servicos_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase_servicos_es', 'Frase Serviços (ES)') !!}
        {!! Form::text('frase_servicos_es', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<hr>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('titulo_quem_faz_pt', 'Título - Quem Faz (PT)') !!}
        {!! Form::text('titulo_quem_faz_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('titulo_quem_faz_en', 'Título - Quem Faz (EN)') !!}
        {!! Form::text('titulo_quem_faz_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('titulo_quem_faz_es', 'Título - Quem Faz (ES)') !!}
        {!! Form::text('titulo_quem_faz_es', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase_quem_faz_pt', 'Frase - Quem Faz (PT)') !!}
        {!! Form::text('frase_quem_faz_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase_quem_faz_en', 'Frase - Quem Faz (EN)') !!}
        {!! Form::text('frase_quem_faz_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('frase_quem_faz_es', 'Frase - Quem Faz (ES)') !!}
        {!! Form::text('frase_quem_faz_es', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('imagem_quem_faz', 'Imagem - Quem Faz') !!}
    @if($home->imagem_quem_faz)
    <img src="{{ url('assets/img/home/'.$home->imagem_quem_faz) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem_quem_faz', ['class' => 'form-control']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('nome_quem_faz', 'Nome - Quem Faz') !!}
    {!! Form::text('nome_quem_faz', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('titulo_cargo_pt', 'Título/Cargo - Quem Faz (PT)') !!}
        {!! Form::text('titulo_cargo_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('titulo_cargo_en', 'Título/Cargo - Quem Faz (EN)') !!}
        {!! Form::text('titulo_cargo_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('titulo_cargo_es', 'Título/Cargo - Quem Faz (ES)') !!}
        {!! Form::text('titulo_cargo_es', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('texto_quem_faz_pt', 'Texto - Quem Faz (PT)') !!}
        {!! Form::textarea('texto_quem_faz_pt', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('texto_quem_faz_en', 'Texto - Quem Faz (EN)') !!}
        {!! Form::textarea('texto_quem_faz_en', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('texto_quem_faz_es', 'Texto - Quem Faz (ES)') !!}
        {!! Form::textarea('texto_quem_faz_es', null, ['class' => 'form-control editor-padrao']) !!}
    </div>
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('home.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>