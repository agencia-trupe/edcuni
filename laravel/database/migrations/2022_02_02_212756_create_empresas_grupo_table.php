<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpresasGrupoTable extends Migration
{
    public function up()
    {
        Schema::create('empresas_grupo', function (Blueprint $table) {
            $table->id();
            $table->integer('ordem')->default(0);
            $table->string('logo');
            $table->string('nome_pt');
            $table->string('nome_en')->nullable();
            $table->string('nome_es')->nullable();
            $table->text('servicos_pt');
            $table->text('servicos_en')->nullable();
            $table->text('servicos_es')->nullable();
            $table->string('link');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('empresas_grupo');
    }
}
