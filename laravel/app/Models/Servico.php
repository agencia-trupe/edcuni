<?php

namespace App\Models;

use App\Helpers\CropImage;
use App\Helpers\CropImageTinify;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Servico extends Model
{
    use HasFactory;

    protected $table = 'servicos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeGrupo($query, $id)
    {
        return $query->where('grupo_id', $id);
    }

    public static function upload_capa()
    {
        if (Configuracao::first()->tinify_key) {
            return CropImageTinify::make('capa', [
                'width'  => 280,
                'height' => 280,
                'path'   => 'assets/img/servicos/'
            ]);
        } else {
            return CropImage::make('capa', [
                'width'  => 280,
                'height' => 280,
                'path'   => 'assets/img/servicos/'
            ]);
        }
    }
}
