@extends('frontend.layout.template')

@section('content')

<main class="grupos">

    <section class="sobre-grupo center">
        <h1 class="titulo">{{ $grupo->{trans('database.frase')} }}</h1>
        <article class="informacoes">
            <div class="icone">
                <img src="{{ asset('assets/img/grupos/'.$grupo->icone) }}" class="img-icone">
            </div>
            <div class="textos">
                <h2 class="titulo-grupo">{{ $grupo->{trans('database.titulo')} }}</h2>
                <div class="texto-grupo">{!! $grupo->{trans('database.texto')} !!}</div>
            </div>
        </article>
    </section>

    <section class="servicos">
        <div class="center">
            @foreach($servicos as $servico)
            <div class="servico">
                <img src="{{ asset('assets/img/servicos/'.$servico->capa) }}" class="img-servico">
                <div class="textos">
                    <h2 class="titulo-servico">{{ $servico->{trans('database.titulo')} }}</h2>
                    <div class="texto-servico">{!! $servico->{trans('database.texto')} !!}</div>
                </div>
            </div>
            @endforeach
        </div>
    </section>

    <section class="grupos" style="background-image: url({{ asset('assets/img/layout/img-base-banner.jpg') }})">
        <div class="center">
            @foreach($grupos as $grupo)
            @if(Lang::getLocale() == "en")
            <a href="{{ route('grupos-en', $grupo->slug_en) }}" class="link-grupo">
                <div class="icone">
                    <img src="{{ asset('assets/img/grupos/'.$grupo->icone) }}" class="img-icone">
                </div>
                <h4 class="titulo-grupo">{{ $grupo->{trans('database.titulo')} }}</h4>
            </a>
            @elseif(Lang::getLocale() == "es")
            <a href="{{ route('grupos-es', $grupo->slug_es) }}" class="link-grupo">
                <div class="icone">
                    <img src="{{ asset('assets/img/grupos/'.$grupo->icone) }}" class="img-icone">
                </div>
                <h4 class="titulo-grupo">{{ $grupo->{trans('database.titulo')} }}</h4>
            </a>
            @else
            <a href="{{ route('grupos', $grupo->slug_pt) }}" class="link-grupo">
                <div class="icone">
                    <img src="{{ asset('assets/img/grupos/'.$grupo->icone) }}" class="img-icone">
                </div>
                <h4 class="titulo-grupo">{{ $grupo->{trans('database.titulo')} }}</h4>
            </a>
            @endif
            @endforeach
        </div>
    </section>

    @include('frontend.contato')

    @include('frontend.grupo-edc')

</main>

@endsection