<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\CapasRequest;
use App\Models\Capa;
use Illuminate\Http\Request;

class CapasController extends Controller
{
    public function index()
    {
        $capas = Capa::ordenados()->get();

        return view('painel.home.capas.index', compact('capas'));
    }

    public function create()
    {
        return view('painel.home.capas.create');
    }

    public function store(CapasRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Capa::upload_capa();

            Capa::create($input);

            return redirect()->route('capas.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Capa $capa)
    {
        return view('painel.home.capas.edit', compact('capa'));
    }

    public function update(CapasRequest $request, Capa $capa)
    {
        try {
            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Capa::upload_capa();

            $capa->update($input);

            return redirect()->route('capas.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Capa $capa)
    {
        try {
            $capa->delete();

            return redirect()->route('capas.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
